package com.example.homeworkspring001;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;


@RestController
public class StaffController {
    ArrayList<StaffInfo> staffInfo = new ArrayList<>();

    public StaffController() {
        staffInfo.add(new StaffInfo(1, "Jame", "Male", 23, "Kandal"));
        staffInfo.add(new StaffInfo(2, "Lundy", "Male", 22, "Prey Veng"));
        staffInfo.add(new StaffInfo(3, "Nary", "Female", 23, "Phnom Penh"));
    }
//Get All Data
    @GetMapping("/api/v1/Customers")
    public ResponseEntity<?> getStaffInfo() {

        return ResponseEntity.ok(new StaffResponse<ArrayList<StaffInfo>>(
                LocalDateTime.now(),
                200,
                "Successfully",
                    staffInfo
        ));
    }


//Insert Data

    @PostMapping("/api/v1/Customers")
    public ResponseEntity<?> insertData(@RequestBody StaffInfo staffInfo1) {
        if (true) {
            staffInfo.add(staffInfo1);
            return ResponseEntity.ok(new StaffResponse(
                    LocalDateTime.now(),
                    200,
                    "Insert Successfully",
                    staffInfo1
            ));
        }else {
            return ResponseEntity.badRequest().body("checking unreachable");
        }

    }


//    Search By ID

    @GetMapping("/api/v1/Customers/{id}")

    public ResponseEntity<?> getStaffInfoById(@PathVariable("id") Integer stfId) {
        for (StaffInfo stf : staffInfo) {
            if (stf.getId() == stfId) {
                return ResponseEntity.ok(new StaffResponse(
                        LocalDateTime.now(),
                        200,
                        "Checking Successfully",
                        stf
                ));
            }

        }
        return ResponseEntity.badRequest().body("checking unreachable");
    }


//    Search By Name

    @GetMapping("/api/v1/Customers/name/")
    @ResponseBody
    public ResponseEntity<?> getStaffByName(@RequestParam String name) {
        for (StaffInfo stf : staffInfo) {
            if (stf.getName().equals(name)) {
                return ResponseEntity.ok(new StaffResponse(
                        LocalDateTime.now(),
                        200,
                        " Checking Successfull",
                            stf
                ));

            }

            }
        return ResponseEntity.badRequest().body("checking unreachable");

        }


//   Update Data

    @PutMapping("/api/v1/Customers/{id}")
    public ResponseEntity<?> getStaffUpdate(@PathVariable ("id") Integer staId, @RequestBody StaffInfo staffInfo2){
        for (int i=0; i<staffInfo.size(); i++) {
            StaffInfo st = staffInfo.get(i);
            if (st.getId().equals(staId)) {
                staffInfo.set(i, staffInfo2);
                    return ResponseEntity.ok(new StaffResponse(
                            LocalDateTime.now(),
                            200,
                            "Update Successfully",
                            staffInfo2
                    ));

            }
        }
        return ResponseEntity.badRequest().body("Data has no this ID !");
    }


//    Deleted Data

    @DeleteMapping("/api/v1/Customers/{id}")
    public ResponseEntity<?> deleteStaff(@PathVariable("id") Integer delId){
       for (int i=0; i<staffInfo.size(); i++){
           StaffInfo st= staffInfo.get(i);
           if (st.getId().equals(delId)){
               staffInfo.remove(i);

               return ResponseEntity.ok(new StaffResponse(
                       LocalDateTime.now(),
                       200,
                       "Deleted Successfully",
                       st
               ));
           }
       }

       return ResponseEntity.badRequest().body("Data has no this ID !");
    }


}






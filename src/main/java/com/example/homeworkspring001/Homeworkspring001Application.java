package com.example.homeworkspring001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homeworkspring001Application {

    public static void main(String[] args) {
        SpringApplication.run(Homeworkspring001Application.class, args);
    }

}
